import { useState } from "react";
import reactLogo from "./assets/react.svg";
import "./App.css";
import { Welcome } from "./pages";

function App() {
  const [count, setCount] = useState(0);

  return (
    <div className="App">
      <Welcome />
    </div>
  );
}

export default App;
