import React from "react";

const Welcome = () => {
  return (
    <div>
      <h1>Welcome to </h1>
      <section>
        <h2>Why</h2>
      </section>
      <section>
        <h2>To whom</h2>
      </section>
      <section>
        <h2>By who</h2>
      </section>
      <section>
        <h2>Motive</h2>
      </section>
      <section>
        <h2>Our Values</h2>
      </section>
    </div>
  );
};

export default Welcome;
