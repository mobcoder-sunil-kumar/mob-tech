const WelcomePage = {
  Why: [
    "We belive in inspration and we want to inspire each other by doing thing that we love.",
  ],
  What: [
    "Top 100 questions",
    "Standered to follow",
    "Interview Preparation",
    "Prof of Concept",
    "Programming",
  ],
  "To Whom": ["We believe in global family", "This is for you"],
  "By Who": ["Mobcoder developers"],
  Motive: ["Information should be free and available to everyone"],
  "Our Values": ["Helping Hand", "Caring", "Accountability"],
  "Why Mobcoder": ["Mobcoder understand the importance of digitilisation"],
};

export { WelcomePage };
